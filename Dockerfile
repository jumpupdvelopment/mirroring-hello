#Build JAR
FROM gradle:7.1.1-jdk11 AS BUILD_IMAGE
RUN mkdir /apps
COPY --chown=gradle:gradle . /apps
WORKDIR /apps
RUN gradle clean build --info jar

FROM openjdk:11-jre
EXPOSE 8080
COPY --from=BUILD_IMAGE /apps/build/libs/*.jar hello.jar
ENTRYPOINT ["java", "-jar", "hello.jar"]
