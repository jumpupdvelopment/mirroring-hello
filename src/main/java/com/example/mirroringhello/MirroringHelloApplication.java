package com.example.mirroringhello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MirroringHelloApplication {

	public static void main(String[] args) {
		SpringApplication.run(MirroringHelloApplication.class, args);
	}

}
